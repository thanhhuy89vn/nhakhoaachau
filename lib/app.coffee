express = require 'express'
config = require 'config'
log4js = require 'log4js'
session = require 'express-session'
request = require 'request'
passport = require 'passport'
mongoose = require 'mongoose'
path = require 'path'
http = require 'http'
LocalStrategy = require('passport-local').Strategy
MongoStore = require('connect-mongo')(session)
## Connect native mongo

MongoClient = require('mongodb').MongoClient


ValidationError = mongoose.Error.ValidationError
ValidatorError= mongoose.Error.ValidatorError

(->
  log4js.setGlobalLogLevel config.logLevel.all
  #console.log 'config.logLevel', config.logLevel

  getLogger = log4js.getLogger
  log4js.getLogger = (name)->
    logger = getLogger.apply log4js, [name]
    level = config.logLevel[name] or config.logLevel.all
    level = config.logLevel.all if !level or level is '^'
    logger.setLevel level
    return logger
)()

logger = log4js.getLogger 'app'

## Init model
models = require './models/index'

app = express()

middlewares = [
  express.static __dirname + './../public/'
  require('cookie-parser')()
  require('body-parser')()
  require('method-override')()
  session {
    secret: config['session'].secret
    store: new MongoStore {
      url : config['session'].mongo
    }
  }
  passport.initialize()
  passport.session()

]
app.use middleware for middleware in middlewares


## app settings
app.set 'port', config.http.port
app.set 'view engine', 'jade'
app.set 'views', __dirname + '/views'
app.set 'view options', {layout: false}
#app.set('view options', { layout: false });

passport = (require './routes/authentication').passport


app.get '/ping', (req, res)->
  res.send('hello nha khoa a chau')

app.use '/', require './routes/router'


## Handle Error
app.use (req, res, next)->
  res.render('404')

###
app.use (err, req, res, next)->
  res.status(err.status || 500)
  console.error "error: " + err.stack
  res.render('500', { error: err })
  ###

noop = ()->
module.exports = {
  app
  start: (cb = noop)->
    @port = @app.get 'port'
    @env = @app.get 'env'
    @http = @app.listen @port
    logger.info "application start listening on port: #{@port} - mode #{@env}"
    cb null

  stop: (cb = noop)->
    @port = null
    @env = null
    @http.close()
    @http = null
    cb null
}


