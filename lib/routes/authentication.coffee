express = require 'express'
LocalStrategy = require('passport-local').Strategy
passport = require 'passport'
config = require 'config'
fibrous = require 'fibrous'
mongoose = require 'mongoose'
log4js = require 'log4js'
logger = log4js.getLogger 'login-api-test'


User = mongoose.model 'user'
passport.use new LocalStrategy(User.authenticate())


## passport session serialization
passport.serializeUser User.serializeUser()
passport.deserializeUser User.deserializeUser()

exports.passport = passport
