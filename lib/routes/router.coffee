express = require 'express'
config = require 'config'
fibrous = require 'fibrous'
mongoose = require 'mongoose'
log4js = require 'log4js'
logger = log4js.getLogger 'router'
User = mongoose.model 'user'
passport = (require './authentication').passport
router = module.exports = express.Router()
ensureLogin = require 'connect-ensure-login'


router.get '/login', (req, res)->
  res.render('login', { user : req.user })

router.post '/login', passport.authenticate('local'), (req, res) ->
  redirectURL = switch req.user.role
    when 'areamanager' then '/stats/vikBirthMonth'
    when 'accounting' then '/card/destroy'
    else '/customer'
  res.redirect(redirectURL)

router.get '/logout', (req, res)->
  req.logout()
  res.redirect('/login')



router.get '/', (req, res) ->
  res.render 'index', {active: 'home'}

router.get '/dich-vu', (req, res) ->
  res.render 'services', {active: 'services'}

router.get '/lien-he', (req, res) ->
  res.render 'contacts', {active: 'contacts'}

router.get '/bang-gia', (req, res) ->
  res.render 'pricing', {active: 'pricing'}

router.get '/hinh-anh', (req, res) ->
  res.render 'photos', {active: 'photos'}

