mongoose = require('mongoose')
passportLocalMongoose = require('passport-local-mongoose')
Schema = mongoose.Schema

schema = new Schema {
  username: String
  password: String
  role:
    type: String
    required: true
    enum: ["normal","moderator","admin"]
}

schema.plugin(passportLocalMongoose);
User = mongoose.model 'user',  schema
