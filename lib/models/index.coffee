config = require 'config'
mongoose = require 'mongoose'
mongoose.models = {}
mongoose.modelSchemas = {}

require '../models/user'

mongoose.connect config.mongo.connection
